# TODO: Converting one file works. If after conversion another file gets loaded, the program crashes.
# Suspected item list may be the culprit

import csv
import sys
import tkinter as tk
import tkinter.filedialog
import re
import webbrowser
import os

from tkinter import ttk

version = "Version: 0.14 210814"
gitRepo = "https://m00gendai@bitbucket.org/m00gendai/csv-to-json-converter"

items = [] # The items list gets cleared to avoid index out of bounds errors
itemLabels = []

def jsonize(): # Initial function, grabs the header and checks the document for errors
    jsonName = jsonFileNameBox.get()
    whereFile = tkinter.filedialog.asksaveasfile(initialdir="/", initialfile=jsonName, title="Save file",
                filetypes=(("Javascript files", "*.js"),("text files", "*.txt"),("all files", "*.*")))
    statusBar.step(10)
    window.update_idletasks() # this is needed to correctly update the progress bar
    window.update()
    fileName = fileInputName.get().rstrip() # rstrip removes the trailing \n
    
    
    if ".csv" not in fileName:
        statusBox.delete(1.0, "end")
        statusBox.insert("end", "Invalid file type. Only CSV allowed.")
        fileInputName.delete(0, "end")
        fileInputName.insert("end", "Only CSV allowed")
        return
    if "." in jsonName: # if someone sets the file name with the extension, it takes that extension
        print(". found")
        print(jsonName)
        jsonName = jsonName
    else: # else it simply adds .js by default
        jsonName = jsonName + ".js"
        print(jsonName)
        

    window.update()
    statusBar.step(10)
    window.update_idletasks()
    
    
        
    window.update()
    trueItems = []
    for itemLabel in itemLabels:
        print(itemLabel.state())
        if itemLabel.instate(['selected']):
            trueItems.append(itemLabel)
    jsonWrite(fileName, trueItems, jsonName) # invoke write function with filename and items list
    window.update()
    statusBar.step(10)
    window.update_idletasks()

    
def jsonWrite(fileName, trueItems, jsonName):
    statusBar.step(10)
    window.update_idletasks()

    try:
        constName = constFileNameBox.get()
        file1 = open(jsonName, "w", encoding='utf-8') # i have no idea why but this seems fine with utf-8 encoding
        file1.write("const " + constName + " = [\n")
        window.update()
        
        with open(fileName, encoding="latin1") as csv_file: # yet here, it only works consistently with latin1 encoding
            csv_reader = csv.reader(csv_file, delimiter=delimiterInputBox.get()) # delimiter is important

            for item in trueItems:
                print(item.cget("text"), str(itemLabels.index(item)))
            
            for row in csv_reader:
                file1.write("{ \n")
                
                for item in trueItems:
                    apData = row[itemLabels.index(item)].replace("\"", "")
                    file1.write(item.cget("text").replace("-", "_") + ": " + "\"" + apData + "\"" + ", \n")
                     

                            

                file1.write("}, \n")
            file1.write("]")


    except Exception as e:
        print(e)
        raise
    
    statusBar.step(10)
    window.update_idletasks()
    window.update()
    file1.close()
    window.update()
    
def openJSON(event):
    chrome_path="C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe %s"
    jsonName = jsonFileNameBox.get()

    if "." in jsonName: # this has to be queried again
        jsonName = jsonName
    else:
        jsonName = jsonName + ".js"
        
    webbrowser.get(chrome_path)
    webbrowser.open(jsonName)

def openFile(event):

 # destroy all widgets from frame
    items = []


    for widget in statusBox.winfo_children()[1:]:
        widget.destroy()
       
    
    # this will clear frame and frame will be empty
    # if you want to hide the empty panel then
  
    
    filePath = tkinter.filedialog.askopenfilename()
    baseName = os.path.basename(filePath)
    
    with open(baseName, encoding='latin1') as csv_file:
        fileInputName.delete(0, "end")
        csv_reader = csv.reader(csv_file) # the delimiter is important
        firstRow = next(csv_reader)
        firstRowString = ''.join(firstRow)
        print(firstRowString)
        delimiterSub = ";"
        
        if delimiterSub in firstRowString: # this checks if ; are present and takes that as a delimiter
            delimiterInputBox.delete(0, "end")
            delimiterInputBox.insert(0, ";")
        else: # if it doesn't find any ;, it assumes comma, as the output is an array of 1 item with all items joined together as one string
            delimiterInputBox.delete(0, "end")
            delimiterInputBox.insert(0, ",")
            
    fileInputName.insert("end", baseName)
    fileInputName.update()
    fileName = fileInputName.get().rstrip() # rstrip removes the trailing \n
    line_count = 0
    item_count = 0
    
    try:
        with open(fileName, encoding='latin1') as csv_file: # idk why, but only latin1 encoding works consistently
            csv_reader = csv.reader(csv_file, delimiter=delimiterInputBox.get()) # the delimiter is important
            for row in csv_reader:
                if line_count < 1: # this ensures only the first line gets read
                    for item in row:
                        items.append(item) # appends each cell to the items list
                        item_count += 1
                    line_count += 1
       # statusBox.insert("end", "Found " + str(item_count) + " items: \n") # displays what headers and how many were found in the gui
        statusBar.step(10)
        window.update_idletasks()
        frameCount = 0
        frameRowCount = 1
        statusLabelVar.set("Found " + str(item_count) + " items:")
        for item in items:
            

            statusFrame = "statusframe" + str(frameCount)
            print(statusFrame)
            statusFrame = ttk.Frame(statusBox)
            statusFrame.grid(row=frameRowCount, column=1)

            itemLabelVar = tk.StringVar()
            itemLabelVar.set(str(item))
            itemLabelText=str(item)
           
            itemLabel = ttk.Checkbutton(statusFrame, text=itemLabelText)
            itemLabel.grid(row=0, column=1)
            itemLabel.state(['!alternate'])
            itemLabel.state(['selected'])
            frameCount += 1
            frameRowCount +=1
            

            statusBox.update()

            itemLabels.append(itemLabel)

            
    except Exception as e:
            print(e)
            raise


def git(event): # git repo link
    webbrowser.open(gitRepo)

def mailto(event): # mail link
    webbrowser.open("mailto:marcel.weber@skyguide.ch?subject=JSONizer&Body=Hi Marcel") 


def helpPop(): # help menu
    rowCount = 0
    helpMenu = tk.Toplevel() # toplevel is a popup window independent from the main window
    helpMenu.title("JSONizer Help")
    
    helpMenuTitle = tk.Label(helpMenu, text="How to use")
    helpMenuTitle.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    helpMenuTitle.configure(font=("Helvetica", 18, "bold"))
    rowCount += 1
    introText = (
        "This tool reads the content of a CSV file and generates a prettyfied JSON file from the data." +
        "\n" +
        "The original CSV remains untouched and a new file with the JSON syntax will be generated." +
        "\n\n" +
        "This tool was developed mainly for the Toolbox V2, as data is often provided in CSV format and manual conversion is time consuming," +
        "\n" +
        "especially when dealing with hundreds of thousands of data rows."
        )
    help0 = tk.Label(helpMenu, text=introText, justify="center")
    help0.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,10),sticky="nesw")
    rowCount += 1
    
    sep0 = ttk.Separator(helpMenu, orient="horizontal")
    sep0.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nsew")
    rowCount += 1

    help1 = tk.Label(helpMenu, text="1. Load CSV File", anchor="w")
    help1.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    help1.configure(font=("Helvetica", 10, "bold"))
    rowCount += 1
    help1text = (
        "Click the \"Load CSV file\"-Button and chose a CSV file from any directory." +
        "\n" +
        "The CSV file name will be displayed next to the button for verification."
        )
    help1content = tk.Label(helpMenu, text=help1text, anchor="w", justify="left")
    help1content.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1
    
    sep1 = ttk.Separator(helpMenu, orient="horizontal")
    sep1.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1
    
    help2 = tk.Label(helpMenu, text="2. Delimiter", anchor="w")
    help2.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    help2.configure(font=("Helvetica", 10, "bold"))
    rowCount += 1
    help2text = (
        "The delimiter is determined by the operating system." +
        "\n" +
        "Usually, it is a \",\" (US) or a \";\" (EUR). The tool detects if \";\" is used and changes the delimiter setting automatically, otherwise it defaults to \",\"." +
        "\n\n" +
        "In the case a different delimiter has been used, for example \"|\", you can change it in the Delimiter input field."
        )
    help2content = tk.Label(helpMenu, text=help2text, anchor="w", justify="left")
    help2content.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1

    sep2 = ttk.Separator(helpMenu, orient="horizontal")
    sep2.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1
    
    help3 = tk.Label(helpMenu, text="3. JSON File Name", anchor="w")
    help3.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    help3.configure(font=("Helvetica", 10, "bold"))
    rowCount += 1
    help3text = (
        "Here you can set how the JSON file shall be named, including setting the file extension." +
        "\n\n" +
        "If no file extension is eplicitly given, it defaults to \".js\"." +
        "\n" +
        "In case only a .txt-file is needed, enter the file name as \"filename.txt\"."
        )
    help3content = tk.Label(helpMenu, text=help3text, anchor="w", justify="left")
    help3content.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1

    sep3 = ttk.Separator(helpMenu, orient="horizontal")
    sep3.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1
    
    help4 = tk.Label(helpMenu, text="4. JSON Object Name", anchor="w")
    help4.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    help4.configure(font=("Helvetica", 10, "bold"))
    rowCount += 1
    help4text = (
        "Here you can set the Object name for the JSON syntax, i.e. \"const <objectName> =\"." +
        "\n" +
        "The default is simply \"jsonObject\", so the JSON syntax would read \"const jsonObject =\"." +
        "\n" +
        "It is strongly recommended to adhere to the camelCase naming convention, e.g. naming constants like \"objectName\" instead of \"objectname\" or \"object_name\"."
        )
    help4content = tk.Label(helpMenu, text=help4text, anchor="w", justify="left")
    help4content.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1

    sep4 = ttk.Separator(helpMenu, orient="horizontal")
    sep4.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1

    help5 = tk.Label(helpMenu, text="5. Convert CSV to JSON", anchor="w")
    help5.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    help5.configure(font=("Helvetica", 10, "bold"))
    rowCount += 1
    help5text = (
        "With \"Convert to JSON\", first you are asked where to save the new JSON file." +
        "\n" +
        "This dialog defaults to saving the file as a .js file, you can change the file type by adding a file extension." +
        "\n" +
        "The CSV file then gets converted to a JSON file (keeping the original CSV untouched)." +
        "\n\n" +
        "In the white display box, all header items will be listed and counted for verification." +
        "\n" +
        "It is advised to use headers in a CSV, as in any case the content of the first row are set as headers." +
        "\n\n" +
        "The box will display a success message at the bottom (you may need to scroll down), or an error will be displayed."
        )
    help5content = tk.Label(helpMenu, text=help5text, anchor="w", justify="left")
    help5content.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1

    sep5 = ttk.Separator(helpMenu, orient="horizontal")
    sep5.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1
    
    help6 = tk.Label(helpMenu, text="6. Show Converted JSON File", anchor="w")
    help6.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    help6.configure(font=("Helvetica", 10, "bold"))
    rowCount += 1
    help6text = (
        "With \"Show converted JSON file\", the generated file will be opened in Notepad for verification."
        )
    help6content = tk.Label(helpMenu, text=help6text, anchor="w", justify="left")
    help6content.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1

    sep6 = ttk.Separator(helpMenu, orient="horizontal")
    sep6.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1

    help7 = tk.Label(helpMenu, text="7. Legal & Contact", anchor="w")
    help7.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    help7.configure(font=("Helvetica", 10, "bold"))
    rowCount += 1
    help7text = (
        "\u00A9 2021 Marcel Weber for AIM Services Zurich" +
        "\n" +
        version
        )
    help7content = tk.Label(helpMenu, text=help7text, anchor="w", justify="left")
    help7content.grid(row=rowCount, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
    rowCount += 1

    help8 = tk.Label(helpMenu, text="git repository: " + gitRepo, anchor="w", cursor="hand1")
    help8.grid(row=rowCount, column=0, padx=(5, 5), pady=(0,0),sticky="nesw")
    help8.bind('<Button-1>', git)
    help8.configure(font=("Helvetica", 10, "underline"), fg="#0000FF")
    rowCount += 1

    help9 = tk.Label(helpMenu, text="Email to Author", anchor="w", cursor="hand1")
    help9.grid(row=rowCount, column=0, padx=(5, 5), pady=(0,20),sticky="nesw")
    help9.bind('<Button-1>', mailto)
    help9.configure(font=("Helvetica", 10, "underline"), fg="#0000FF")
    rowCount += 1

window = tk.Tk()
window.title("JSONizer")
window.minsize(400, 200)
window.resizable(False, False)



welcomeLabel = ttk.Label(text="Welcome to weberml's CSV to JSON converter!")
welcomeLabel.grid(row=0, column=0, columnspan=2, padx=5, pady=5, sticky="nesw")
welcomeLabel.configure(anchor="center")

fileInputBox = ttk.Button(text="Load CSV file", command=jsonize, cursor="based_arrow_up")
fileInputBox.grid(row=1, column=0, padx=(5, 5), pady=(5,0),sticky="nesw")
fileInputBox.bind('<Button-1>', openFile)

fileInputName = ttk.Entry()
fileInputName.grid(row=1, column=1, padx=(5, 5), pady=(5,0),sticky="nesw")
fileInputName.insert(0, "")

delimiterLabel = ttk.Label(text="Delimiter (Change only if different):")
delimiterLabel.grid(row=2, column=0, padx=5, pady=5,sticky="nesw")
delimiterLabel.configure(anchor="center")

delimiterInputBox = ttk.Entry()
delimiterInputBox.grid(row=2, column=1, padx=(5, 5), pady=(5,0),sticky="nesw")
delimiterInputBox.insert(0, ";")

jsonFileNameLabel = ttk.Label(text="Desired JSON file name: ")
jsonFileNameLabel.grid(row=3, column=0, padx=5, pady=5,sticky="nesw")
jsonFileNameLabel.configure(anchor="center")

jsonFileNameBox = ttk.Entry()
jsonFileNameBox.grid(row=3, column=1, padx=(5, 5), pady=(5,0),sticky="nesw")
jsonFileNameBox.insert(0, "json")

constNameLabel = ttk.Label(text="Desired Object name: ")
constNameLabel.grid(row=4, column=0, padx=5, pady=5,sticky="nesw")
constNameLabel.configure(anchor="center")

constFileNameBox = ttk.Entry()
constFileNameBox.grid(row=4, column=1, padx=(5, 5), pady=(5,0),sticky="nesw")
constFileNameBox.insert(0, "jsonObject")

convertButton = ttk.Button(text="Convert to JSON", command=jsonize, cursor="exchange")
convertButton.grid(row=97, column=0, columnspan=2, padx=5, pady=10, sticky="ew")

statusBar = ttk.Progressbar(orient="horizontal", mode="determinate", value=0, length=400, maximum=60)
statusBar.grid(row=98, column=0, columnspan=2, padx=5, pady=10, sticky="ew")

statusBox = ttk.Frame()
statusBox.grid(row=99, column=0, columnspan=2, padx=(5, 5), sticky="w")

statusLabelVar = tk.StringVar()
statusLabelVar.set("Waiting for CSV file")
statusLabel = ttk.Label(statusBox, textvariable=statusLabelVar)
statusLabel.grid(row=0, column=1)


showButton = ttk.Button(text="Show converted JSON file", cursor="target")
showButton.grid(row=100, column=0, columnspan=2, padx=5, pady=10, sticky="ew")
showButton.bind('<Button-1>', openJSON)

exitButton = ttk.Button(text="Exit", command=window.destroy, cursor="pirate")
exitButton.grid(row=101, column=1, padx=5, pady=(0,10), sticky="ew")

helpButton = ttk.Button(text="Help & Contact", command=helpPop, cursor="question_arrow")
helpButton.grid(row=101, column=0, padx=5, pady=(0,10), sticky="ew")

sepMain = ttk.Separator(window, orient="horizontal")
sepMain.grid(row=102, column=0, columnspan=2, padx=(5, 5), pady=(5,0),sticky="nesw")

versionHist = tk.Label(text="\u00A9 2021 Marcel Weber for AIM Services Zurich, " + version)
versionHist.grid(row=103, column=0, columnspan=2, padx=(5, 5), pady=(5,10),sticky="nesw")


window.mainloop()
