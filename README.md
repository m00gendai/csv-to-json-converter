# README #


### Purpose ###

This tool converts CSV files into prettyfied JSON files to be used with Javascript. 

It automatically gets the first row of the CSV document and sets the items as object keys and every item from a subsequent row as the key values.
Currently there is no option to set individual headers, so headerless CSV files are not recommended.

Custom delimiters are possible. 

The tool reads back the key items found and lists them, including how many there are.

Errors are handled.

After successful conversion, there is an option to display the JSON file.

### Setup ###

Written in Python 3.9. All standard Python libraries, no external resources/libraries need to be installed.
The CSV file must be in the same directory as the Python file.

Libraries used:

*	csv - to read CSV files
*	time - not used at this moment
*	sys - to handle system calls
*	random - not used at this moment
*	tkinter - to create a GUI
*	re - for regex pattern matching
*	webbrowser - to open files

### Contribution guidelines ###

None

### Who do I talk to? ###

Repo owner or admin